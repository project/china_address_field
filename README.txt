README.txt
==========

INTRODUCTION
------------
A Field offering province, city, and county of China.

Installlation
------------
1. Copy China_address_field folder to modules directory (usually sites/all/modules).

2. At admin/modules enable the China address module in the Fields 
   package.

3. In the field setting page, choose what kind of level you need to display 
   the field.

Author
-------

Webonn <developer at webonn DOT com>
http://webonn.com
